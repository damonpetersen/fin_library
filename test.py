from finance_byu.regtables import Regtable
import pandas as pd
import statsmodels.formula.api as smf
import numpy as np

nobs = 1000
df = pd.DataFrame(np.random.random((nobs,3))).rename(columns={0:'age',1:'bmi',2:'hincome'})
df['age'] = df['age']*100
df['bmi'] = df['bmi']*30
df['hincome'] = df['hincome']*100000
df['hincome'] = pd.qcut(df['hincome'],16,labels=False)
df['rich'] = df['hincome'] > 13
df['gender'] = np.random.choice(['M','F'],nobs)
df['race'] = np.random.choice(['W','B','H','O'],nobs)

regformulas =  ['bmi ~ age',
                'bmi ~ np.log(age)',
                'bmi ~ C(gender) + np.log(age)',
                'bmi ~ C(gender) + C(race) + np.log(age)',
                'bmi ~ C(gender) + rich + C(gender)*rich + C(race) + np.log(age)',
                'bmi ~ -1 + np.log(age)',
                'bmi ~ -1 + C(race) + np.log(age)']
reg = [smf.ols(f,df).fit() for f in regformulas]
tbl = Regtable(reg)
tbl.orientation = 'horizontal'
tbl.set_reg_names('num-brackets')
r = tbl.render()