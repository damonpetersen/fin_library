#!/usr/bin/env python3c
import pandas as pd
import numpy as np
from tabulate import tabulate
from finance_byu.rolling import roll_std

    
def past_sigma(win=250,minp=125):
    dstk = pd.read_hdf('data/dstk_3.h5')[['permno','caldt','ret']]    
    dstk['caldt'] = dstk['caldt'].values.astype('datetime64[D]')
    
    std = lambda x: roll_std(x,win=win,minp=minp,errors='return')
    dstk['sigma'] = dstk.groupby('permno')['ret'].transform(std)    
    
    dstk['mdt'] = dstk['caldt'].values.astype('datetime64[M]')
    dstk = dstk.drop_duplicates(subset=['permno','mdt'],keep='last')
    
    return dstk.drop(['ret','mdt'],axis=1)


def main():
    stk = pd.read_hdf('data/mstk_3.h5')    
    stk['caldt'] = stk['caldt'].values.astype('datetime64[D]')

    stk['prc'] = stk.eval("abs(prc)")
    stk['me']  = stk.eval("prc*shr/1000.0").where(stk.eval("prc*shr > 1e-6"))
    
    stk = stk.merge(past_sigma(250,125),how='left',on=['permno','caldt'])

    cols = ['me','prc','sigma']  
    stk[cols] = stk.groupby('permno')[cols].shift()

    stk = stk.query("10 <= shrcd <= 11 and me == me and sigma == sigma and "
                    "prc > 4.999 and ret == ret").reset_index(drop=True)

    cut = lambda x: x.quantile(np.arange(.2,1,.2)).searchsorted(x)
    stk['port'] = stk.groupby('caldt')['sigma'].transform(cut)

    ew = (stk.pivot_table('ret','caldt',columns='port',aggfunc='mean')
          .rename('p{:.0f}'.format,axis='columns')*100)
    print(ew.describe())
    
if __name__ == '__main__':
    main()
