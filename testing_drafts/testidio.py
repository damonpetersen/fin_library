#!/usr/bin/env python3c
import pandas as pd
import numpy as np
from finance_byu.rolling import roll_idio


def past_idio():
    dstk = pd.read_hdf('dstk_3.h5')[['permno','caldt','ret']]
    dstk['caldt'] = dstk['caldt'].values.astype('datetime64[D]')

    dsi = pd.read_csv('dsi.csv',parse_dates=['caldt'],index_col=['caldt'])

    dstk = dstk.set_index('caldt').assign(mkt = dsi['vwretd']).reset_index()
    dstk['mkt'] = dstk['mkt'].where(dstk['ret'].notnull())

    idio_std = lambda x: roll_idio(x['ret'],x['mkt'],250,125,errors='return')
    dstk['idio'] = dstk.groupby('permno')[['ret','mkt']].apply(idio_std)

    dstk['mdt'] = dstk['caldt'].values.astype('datetime64[M]')
    dstk = dstk.drop_duplicates(subset=['permno','mdt'],keep='last')

    return dstk.drop(['ret','mkt','mdt'],axis=1)


def main():
    stk = pd.read_hdf('mstk_3.h5')    
    stk['caldt'] = stk['caldt'].values.astype('datetime64[D]')

    stk['prc'] = stk.eval("abs(prc)")
    stk['me']  = stk.eval("prc*shr/1000.0").where(stk.eval("prc*shr > 1e-6"))
    
    stk = stk.merge(past_idio(),how='left',on=['permno','caldt'])

    cols = ['me','prc','idio']  
    stk[cols] = stk.groupby('permno')[cols].shift()
    

    stk = (stk.query('10 <= shrcd <= 11 and me == me and idio == idio')
           .reset_index(drop=True))
    cut = lambda x: x.quantile(np.arange(.2,1,.2)).searchsorted(x)
    stk['port'] = stk.groupby('caldt')['idio'].transform(cut)
    
    stk = stk.query('prc > 4.999 and ret == ret').reset_index(drop=True)

    
    ew = (stk.pivot_table('ret','caldt',columns='port',aggfunc='mean')
          .rename('p{:.0f}'.format,axis='columns')*100)

    print(ew.describe())


    
if __name__ == '__main__':
    main()
