import setuptools

with open("README.md","r") as f:
    mylong_description = f.read()

setuptools.setup(   name='finance_byu',
                    version='0.1.000.28',
                    #py_modules=['./finance_byu/__init__','./finance_byu/regtables'],
                    packages=['finance_byu'],
                    package_data={'':['*.txt']},
                    classifiers=["Programming Language :: Python :: 3",
                                 "Operating System :: OS Independent"],
                    description="Classes and functions for FIN 585 and finance research at \
                                 Brigham Young University",
                    long_description=mylong_description,
                    long_description_content_type="text/markdown",
                    install_requires = ['numpy',
                                        'pandas',
                                        'joblib',
                                        'statsmodels',
                                        'numba',
                                        'scipy',
                                        'patsy'],
                    license='MIT',
                    python_requires='>=3.6',
                    author="Damon Petersen"
                )