Statistics Module
=================

The statistics module contains the :code:`GRS` function which computes the Gibbons, Ross, and Shanken (GRS) 
test for a factor model.

:mod:`statistics`
-----------------------

.. autofunction:: finance_byu.statistics.GRS

Example
--------

::

    >>> from finance_byu.statistics import GRS
    >>> import numpy as np
    >>> import pandas as pd
    >>> 
    >>> n_periods = 1.0e2
    >>> 
    >>> df = pd.DataFrame(np.random.random((int(n_periods),6)))
    >>> df = df.rename(columns={0:'port1',1:'port2',2:'port3',3:'exmkt',4:'smb',5:'hml'})
    >>> grsstat,pval,tbl = GRS(df,['port1','port2','port3'],['exmkt','smb','hml'])
    >>> grsstat
    
    29.257621230624505
    
    >>> pval
    
    1.8957078244964134e-13
    
    >>> from tabulate import tabulate
    >>> print(tabulate(tbl.render(),tablefmt='github',headers=tbl.render().columns))
    
    |           | port1   | port2   | port3   |
    |-----------|---------|---------|---------|
    | Intercept | 0.431   | 0.574   | 0.452   |
    |           | (4.78)  | (6.82)  | (5.14)  |
    | exmkt     | -0.020  | 0.075   | 0.146   |
    |           | (-0.19) | (0.79)  | (1.48)  |
    | smb       | 0.094   | 0.051   | -0.135  |
    |           | (0.91)  | (0.53)  | (-1.32) |
    | hml       | 0.064   | -0.258  | 0.009   |
    |           | (0.62)  | (-2.69) | (0.09)  |
    | Obs       | 100     | 100     | 100     |
    | Rsq       | 0.01    | 0.08    | 0.04    |