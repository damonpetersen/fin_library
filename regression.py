import pandas as pd
import statsmodels.formula.api as smf
import numpy as np

df = pd.read_csv('./01-nhanes.csv')
df = df.query("age >= 18 and bmi == bmi")
df = df.reset_index(drop=True)
df['rich'] = df['hincome'] == 15


def _stack(r,stat,stars,bfmt,sfmt):
    est = 'bstar' if stars else 'b'

    b = r.params.apply(lambda x: '{:{fmt}}'.format(x,fmt=bfmt))
    se = r.bse.apply(lambda x: '({:{fmt}})'.format(x,fmt=sfmt))
    tstat = r.tvalues.apply(lambda x: '({:{fmt}})'.format(x,fmt=sfmt))
    pvalues = r.pvalues.apply(lambda x: '({:{fmt}})'.format(x,fmt=sfmt))        
    bstar = pd.cut(r.pvalues,[-0.01,0.01,0.05,1.01],labels=['***','**',''])
    bstar = b + bstar.astype(str)
    
    return (pd.concat([b,se,tstat,pvalues,bstar],axis=1)
            .rename(columns={0:'b',1:'se',2:'tstat',3:'pvalues',4:'bstar'})
            [[est,stat]].stack())


def regtable_fcn(lm,stat='tstat',stars=False,bfmt='.3f',sfmt='.2f'):
    """
    Combines a list of statsmodels regression results in a single table.
    
    regtable stacks the results from each regression  into a single column. 
    T-statistics in parentheses (by default) are interleaved below the
    estimated coefficient. The number of observation and r-square of the
    each regression are also appended.

    Parameters
    ----------
    lm    : a list of statmodels results objects.
    stat  : {'tstat','se','pvalues'}, default is 'tstat'
        Determines whether t-stats, standard errors, or p-values are reported.
    stars : boolean, default False
        if True, significance stars at the 1% (***) and the 5% (**) level are 
        added to the table 
    bmft  : string, default = '.3f'
        Float formatting string for the estimated coefficients
    smft  : string, default = '.2f'
        Float formatting string for the stat paramater (e.g., t-statistics or 
        standard errors)

    Returns
    -------
    out : DataFrame 
        A columnar table where each column corresponds to the results of a
        estimated regression model from statsmodels.

    Examples
    --------
    Suppose ew contains excess portfolio returns, p0 is one of the portfolio
    columns, and ew also contains the Fama-French factors and a momentum 
    factor. In the example below we run a three different factor model 
    regressions and collect the results into a table using regtable. The
    example makes use of the tabulate package to format the regtable dataframe
    for printing.

    >>> r0 = smf.ols('p0 ~ exmkt',data=ew).fit()
    >>> r1 = smf.ols('p0 ~ exmkt + smb + hml',data=ew).fit()
    >>> r2 = smf.ols('p0 ~ exmkt + smb + hml + umd',data=ew).fit()
    >>>
    >>> tabulate(regtable([r0,r1,r2]),headers='keys',tablefmt='simple')

               p0       p0       p0
    ---------  -------  -------  -------
    Intercept  0.352    0.317    0.298
               (7.71)   (7.22)   (6.61)
    exmkt      0.677    0.646    0.650
               (79.80)  (73.61)  (71.78)
    hml                 0.102    0.111
                        (7.93)   (8.06)
    smb                 0.079    0.080
                        (5.51)   (5.56)
    umd                          0.019
                                 (1.82)
    Obs        1098     1098     1098
    RSQ        0.85     0.87     0.87
    """
    
    cols = [x.model.endog_names for x in lm]    
    res = [_stack(r,stat,stars,bfmt,sfmt) for r in lm]

    out = pd.concat(res,axis=1,keys=cols).reset_index()
    out.loc[out['level_1'] == stat,'level_0'] = ''
    out = out.drop('level_1',axis=1).set_index('level_0')
    out.index.name = ""

    obs = pd.Series([int(r.nobs) for r in lm],cols,name='Obs')
    rsq = pd.Series(['{:{fmt}}'.format(r.rsquared,fmt=sfmt) for r in lm],cols,
                    name='Rsq')
    
    return out.append(obs).append(rsq).fillna('')

reg0 = smf.ols('bmi ~ age',data=df).fit()
reg1 = smf.ols('bmi ~ np.log(age)',data=df).fit()
reg2 = smf.ols('bmi ~ C(gender) + np.log(age)',data=df).fit()
reg3 = smf.ols('bmi ~ C(gender) + C(race) + np.log(age)',data=df).fit()
reg4 = smf.ols('bmi ~ C(gender) + rich + C(gender)*rich + C(race) + np.log(age)',data=df).fit()
reg5 = smf.ols('bmi ~ -1 + np.log(age)',data=df).fit()
reg6 = smf.ols('bmi ~ -1 + C(race) + np.log(age)',data=df).fit()
mytbl = regtable_fcn([reg0,reg1,reg2,reg3,reg4,reg5,reg6],stat='tstat',stars=True)