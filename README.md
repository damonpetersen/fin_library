# BYU FIN 585 Library (finance_byu)

This library contains classes and functions for FIN 585 at Brigham Young University which are
generally useful for academic finance research.

## Getting Started

The library can currently be installed using the following command: `pip install finance-byu`.

To use this library, use the following code at the top of your script: `import finance_byu as fin`. 

### Prerequisites

You should have Continuum Anaconda installed prior to installing this package.

## License

This project is licensed under the MIT License.