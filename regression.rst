Regression Module
==================

The regression module contains a function for fixed effects regression similar to Stata's :code:`areg` function. 

:mod:`regression`
-----------------------

.. autofunction:: finance_byu.regression.areg

Example
--------

::

    >>> from finance_byu.regression import areg
    >>> import numpy as np
    >>> import pandas as pd
    >>> 
    >>> n_obs = 1.0e2
    >>> 
    >>> df = pd.DataFrame(np.random.random((int(n_obs),4)))
    >>> df = df.rename(columns={0:'ret',1:'beta',2:'logme',3:'logbm'})
    >>> df['caldt'] = np.random.choice(10,n_obs)
    >>> reg = areg('ret ~ beta + logme + logbm',data=df,absorb='caldt',cluster='caldt')
    >>> reg.summary()
    
                                    OLS Regression Results                            
    ==============================================================================
    Dep. Variable:                    ret   R-squared:                       0.027
    Model:                            OLS   Adj. R-squared:                 -0.107
    Method:                 Least Squares   F-statistic:                     2.136
    Date:                Sat, 14 Dec 2019   Prob (F-statistic):              0.166
    Time:                        14:01:32   Log-Likelihood:                -7.8543
    No. Observations:                 100   AIC:                             23.71
    Df Residuals:                      87   BIC:                             34.13
    Df Model:                           3                                         
    Covariance Type:              cluster                                         
    ==============================================================================
                     coef    std err          z      P>|z|      [0.025      0.975]
    ------------------------------------------------------------------------------
    Intercept      0.5400      0.089      6.095      0.000       0.366       0.714
    beta           0.0480      0.045      1.062      0.288      -0.041       0.137
    logme         -0.1484      0.074     -2.013      0.044      -0.293      -0.004
    logbm          0.0243      0.121      0.200      0.841      -0.213       0.262
    ==============================================================================
    Omnibus:                       24.428   Durbin-Watson:                   2.069
    Prob(Omnibus):                  0.000   Jarque-Bera (JB):                5.333
    Skew:                           0.033   Prob(JB):                       0.0695
    Kurtosis:                       1.871   Cond. No.                         6.82
    ==============================================================================

    Warnings:
    [1] Standard Errors are robust to cluster correlation (cluster)
   