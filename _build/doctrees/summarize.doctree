��|6      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Summarize Module�h]�h �Text����Summarize Module�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�0C:\Users\rpham\Desktop\FIN_Library\summarize.rst�hKubh �	paragraph���)��}�(h��The summarize module contains the :code:`summarize` function which is useful for
creating summary tables for DataFrames which contain portfolio returns.�h]�(h�"The summarize module contains the �����}�(h�"The summarize module contains the �hh-hhhNhNubh �literal���)��}�(h�:code:`summarize`�h]�h�	summarize�����}�(h�	summarize�hh8ubah}�(h]�h!]��code�ah#]�h%]�h']�uh)h6hh-ubh�e function which is useful for
creating summary tables for DataFrames which contain portfolio returns.�����}�(h�e function which is useful for
creating summary tables for DataFrames which contain portfolio returns.�hh-hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h�:mod:`summarize`�h]��sphinx.addnodes��pending_xref���)��}�(hhXh]�h7)��}�(hhXh]�h�	summarize�����}�(hhhh`ubah}�(h]�h!]�(�xref��py��py-mod�eh#]�h%]�h']�uh)h6hh]ubah}�(h]�h!]�h#]�h%]�h']��refdoc��	summarize��	refdomain�hk�reftype��mod��refexplicit���refwarn���	py:module�N�py:class�N�	reftarget��	summarize�uh)h[hh*hKhhVubah}�(h]�h!]�h#]�h%]�h']�uh)hhhShhhh*hKubhZ�index���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']��entries�]�(�single��-summarize() (in module finance_byu.summarize)��finance_byu.summarize.summarize�hNt�auh)h�hhShhhNhNubhZ�desc���)��}�(hhh]�(hZ�desc_signature���)��}�(h�Csummarize(df, pvalues=False, std=True, count=True, quartiles=False)�h]�(hZ�desc_addname���)��}�(h�finance_byu.summarize.�h]�h�finance_byu.summarize.�����}�(hhhh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)h�hh�hhh�hC:\Users\rpham\Desktop\FIN_Library\finance_byu\summarize.py:docstring of finance_byu.summarize.summarize�hNubhZ�	desc_name���)��}�(h�	summarize�h]�h�	summarize�����}�(hhhh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�hhhh�hNubhZ�desc_parameterlist���)��}�(h�8df, pvalues=False, std=True, count=True, quartiles=False�h]�(hZ�desc_parameter���)��}�(h�df�h]�h�df�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�pvalues=False�h]�h�pvalues=False�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�std=True�h]�h�std=True�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�
count=True�h]�h�
count=True�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�quartiles=False�h]�h�quartiles=False�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�hhhh�hNubeh}�(h]�h�ah!]�h#]�h�ah%]�h']��first���module��finance_byu.summarize��class�h�fullname�h�uh)h�hh�hhhh�hNubhZ�desc_content���)��}�(hhh]�(h,)��}�(h�0Compute a summary for a dataframe of portfolios.�h]�h�0Compute a summary for a dataframe of portfolios.�����}�(hj,  hj*  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+h�hC:\Users\rpham\Desktop\FIN_Library\finance_byu\summarize.py:docstring of finance_byu.summarize.summarize�hKhj'  hhubh �
field_list���)��}�(hhh]�(h �field���)��}�(hhh]�(h �
field_name���)��}�(h�
Parameters�h]�h�
Parameters�����}�(hhhjE  ubah}�(h]�h!]�h#]�h%]�h']�uh)jC  hj@  hj8  hK ubh �
field_body���)��}�(hXm  **df: pandas.core.frame.DataFrame**
    DataFrame with portfolio returns in each column.

**pvalues: bool**
    Whether to report pvalues. 

**std: bool**
    Whether to report the standard deviations of portfolio returns.

**count: bool**
    Whether to report the return count for each portfolio.

**quartiles: bool**
    Whether to report quartiles for returns.
�h]�h �definition_list���)��}�(hhh]�(h �definition_list_item���)��}�(h�U**df: pandas.core.frame.DataFrame**
DataFrame with portfolio returns in each column.
�h]�(h �term���)��}�(h�#**df: pandas.core.frame.DataFrame**�h]�h �strong���)��}�(hjh  h]�h�df: pandas.core.frame.DataFrame�����}�(hhhjl  ubah}�(h]�h!]�h#]�h%]�h']�uh)jj  hjf  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hKhj`  ubh �
definition���)��}�(hhh]�h,)��}�(h�0DataFrame with portfolio returns in each column.�h]�h�0DataFrame with portfolio returns in each column.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj`  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hKhj[  ubj_  )��}�(h�.**pvalues: bool**
Whether to report pvalues. 
�h]�(je  )��}�(h�**pvalues: bool**�h]�jk  )��}�(hj�  h]�h�pvalues: bool�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jj  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hKhj�  ubj�  )��}�(hhh]�h,)��}�(h�Whether to report pvalues.�h]�h�Whether to report pvalues.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hKhj[  ubj_  )��}�(h�N**std: bool**
Whether to report the standard deviations of portfolio returns.
�h]�(je  )��}�(h�**std: bool**�h]�jk  )��}�(hj�  h]�h�	std: bool�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jj  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hKhj�  ubj�  )��}�(hhh]�h,)��}�(h�?Whether to report the standard deviations of portfolio returns.�h]�h�?Whether to report the standard deviations of portfolio returns.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hKhj[  ubj_  )��}�(h�G**count: bool**
Whether to report the return count for each portfolio.
�h]�(je  )��}�(h�**count: bool**�h]�jk  )��}�(hj  h]�h�count: bool�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)jj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hKhj  ubj�  )��}�(hhh]�h,)��}�(h�6Whether to report the return count for each portfolio.�h]�h�6Whether to report the return count for each portfolio.�����}�(hj.  hj,  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhj)  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hKhj[  ubj_  )��}�(h�=**quartiles: bool**
Whether to report quartiles for returns.
�h]�(je  )��}�(h�**quartiles: bool**�h]�jk  )��}�(hjL  h]�h�quartiles: bool�����}�(hhhjN  ubah}�(h]�h!]�h#]�h%]�h']�uh)jj  hjJ  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hKhjF  ubj�  )��}�(hhh]�h,)��}�(h�(Whether to report quartiles for returns.�h]�h�(Whether to report quartiles for returns.�����}�(hjf  hjd  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhja  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjF  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hKhj[  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jY  hjU  ubah}�(h]�h!]�h#]�h%]�h']�uh)jS  hj@  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j>  hj8  hKhj;  hhubj?  )��}�(hhh]�(jD  )��}�(h�Returns�h]�h�Returns�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jC  hj�  hj8  hK ubjT  )��}�(hX  output: pandas.core.frame.DataFrame
    Summary DataFrame including by default t-statistics testing that the mean returns for each portfolio are zero, 
    the count for each portfolio, the mean return for each portfolio, and the standard deviation for each 
    portfolio. 












�h]�jZ  )��}�(hhh]�j_  )��}�(hX  output: pandas.core.frame.DataFrame
Summary DataFrame including by default t-statistics testing that the mean returns for each portfolio are zero, 
the count for each portfolio, the mean return for each portfolio, and the standard deviation for each 
portfolio. 












�h]�(je  )��}�(h�#output: pandas.core.frame.DataFrame�h]�h�#output: pandas.core.frame.DataFrame�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jd  hj8  hK'hj�  ubj�  )��}�(hhh]�h,)��}�(h��Summary DataFrame including by default t-statistics testing that the mean returns for each portfolio are zero, 
the count for each portfolio, the mean return for each portfolio, and the standard deviation for each 
portfolio.�h]�h��Summary DataFrame including by default t-statistics testing that the mean returns for each portfolio are zero, 
the count for each portfolio, the mean return for each portfolio, and the standard deviation for each 
portfolio.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj8  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j^  hj8  hK'hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jY  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jS  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j>  hj8  hKhj;  hhubeh}�(h]�h!]�h#]�h%]�h']�uh)j9  hj'  hhhNhNubh �comment���)��}�(h�!! processed by numpydoc !!�h]�h�!! processed by numpydoc !!�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)j�  hj'  hhhj8  hK*ubeh}�(h]�h!]�h#]�h%]�h']�uh)j%  hh�hhhh�hNubeh}�(h]�h!]�h#]�h%]�h']��domain��py��objtype��function��desctype�j  �noindex��uh)h�hhhhShNhNubeh}�(h]��	summarize�ah!]�h#]��	summarize�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Examples�h]�h�Examples�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj  hhhh*hKubh �literal_block���)��}�(hX�  >>> import pandas as pd
>>> import finance_byu.rolling as rolling
>>> import numpy as np
>>>
>>> n_periods = 1.0e2
>>>
>>> df = pd.DataFrame(np.random.random((int(n_periods),4)))
>>> df = df.rename(columns={0:'Port1',1:'Port2',2:'Port3',3:'Port4'})
>>> summarize(df)

            Port1       Port2       Port3       Port4
count  100.000000  100.000000  100.000000  100.000000
mean     0.497998    0.534308    0.487955    0.465950
std      0.279735    0.271203    0.298592    0.303237
tstat   17.802506   19.701371   16.341894   15.365854

>>> summarize(df,pvalues=True,std=False,count=False,quartiles=True)

              Port1         Port2         Port3         Port4
min    1.638275e-02  1.067668e-02  8.859616e-03  3.235157e-03
25%    2.660066e-01  3.380128e-01  2.063304e-01  2.157772e-01
50%    5.131368e-01  5.222941e-01  5.020994e-01  4.217088e-01
75%    7.373673e-01  7.453572e-01  7.365743e-01  7.295367e-01
max    9.747179e-01  9.987271e-01  9.922892e-01  9.813587e-01
mean   4.979984e-01  5.343077e-01  4.879551e-01  4.659497e-01
tstat  1.780251e+01  1.970137e+01  1.634189e+01  1.536585e+01
pval   1.268239e-32  4.967867e-36  7.213884e-30  5.814044e-28�h]�hX�  >>> import pandas as pd
>>> import finance_byu.rolling as rolling
>>> import numpy as np
>>>
>>> n_periods = 1.0e2
>>>
>>> df = pd.DataFrame(np.random.random((int(n_periods),4)))
>>> df = df.rename(columns={0:'Port1',1:'Port2',2:'Port3',3:'Port4'})
>>> summarize(df)

            Port1       Port2       Port3       Port4
count  100.000000  100.000000  100.000000  100.000000
mean     0.497998    0.534308    0.487955    0.465950
std      0.279735    0.271203    0.298592    0.303237
tstat   17.802506   19.701371   16.341894   15.365854

>>> summarize(df,pvalues=True,std=False,count=False,quartiles=True)

              Port1         Port2         Port3         Port4
min    1.638275e-02  1.067668e-02  8.859616e-03  3.235157e-03
25%    2.660066e-01  3.380128e-01  2.063304e-01  2.157772e-01
50%    5.131368e-01  5.222941e-01  5.020994e-01  4.217088e-01
75%    7.373673e-01  7.453572e-01  7.365743e-01  7.295367e-01
max    9.747179e-01  9.987271e-01  9.922892e-01  9.813587e-01
mean   4.979984e-01  5.343077e-01  4.879551e-01  4.659497e-01
tstat  1.780251e+01  1.970137e+01  1.634189e+01  1.536585e+01
pval   1.268239e-32  4.967867e-36  7.213884e-30  5.814044e-28�����}�(hhhj,  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)j*  hKhj  hhhh*ubeh}�(h]��examples�ah!]�h#]��examples�ah%]�h']�uh)h	hhhhhh*hKubeh}�(h]��summarize-module�ah!]�h#]��summarize module�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jm  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jG  jD  j  j  h�h�j?  j<  u�	nametypes�}�(jG  Nj  Nh��j?  Nuh}�(jD  hj  hSh�h�j<  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.