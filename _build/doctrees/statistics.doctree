���1      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Statistics Module�h]�h �Text����Statistics Module�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�1C:\Users\rpham\Desktop\FIN_Library\statistics.rst�hKubh �	paragraph���)��}�(h��The statistics module contains the :code:`GRS` function which computes the Gibbons, Ross, and Shanken (GRS)
test for a factor model.�h]�(h�#The statistics module contains the �����}�(h�#The statistics module contains the �hh-hhhNhNubh �literal���)��}�(h�:code:`GRS`�h]�h�GRS�����}�(h�GRS�hh8ubah}�(h]�h!]��code�ah#]�h%]�h']�uh)h6hh-ubh�V function which computes the Gibbons, Ross, and Shanken (GRS)
test for a factor model.�����}�(h�V function which computes the Gibbons, Ross, and Shanken (GRS)
test for a factor model.�hh-hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h�:mod:`statistics`�h]��sphinx.addnodes��pending_xref���)��}�(hhXh]�h7)��}�(hhXh]�h�
statistics�����}�(hhhh`ubah}�(h]�h!]�(�xref��py��py-mod�eh#]�h%]�h']�uh)h6hh]ubah}�(h]�h!]�h#]�h%]�h']��refdoc��
statistics��	refdomain�hk�reftype��mod��refexplicit���refwarn���	py:module�N�py:class�N�	reftarget��
statistics�uh)h[hh*hKhhVubah}�(h]�h!]�h#]�h%]�h']�uh)hhhShhhh*hKubhZ�index���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']��entries�]�(�single��(GRS() (in module finance_byu.statistics)��finance_byu.statistics.GRS�hNt�auh)h�hhShhhNhNubhZ�desc���)��}�(hhh]�(hZ�desc_signature���)��}�(h�GRS(df, exret, factors)�h]�(hZ�desc_addname���)��}�(h�finance_byu.statistics.�h]�h�finance_byu.statistics.�����}�(hhhh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)h�hh�hhh�dC:\Users\rpham\Desktop\FIN_Library\finance_byu\statistics.py:docstring of finance_byu.statistics.GRS�hNubhZ�	desc_name���)��}�(h�GRS�h]�h�GRS�����}�(hhhh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�hhhh�hNubhZ�desc_parameterlist���)��}�(h�df, exret, factors�h]�(hZ�desc_parameter���)��}�(h�df�h]�h�df�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�exret�h]�h�exret�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubh�)��}�(h�factors�h]�h�factors�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�ubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�hhhh�hNubeh}�(h]�h�ah!]�h#]�h�ah%]�h']��first���module��finance_byu.statistics��class�h�fullname�h�uh)h�hh�hhhh�hNubhZ�desc_content���)��}�(hhh]�(h,)��}�(h�OImplementation of the Gibbons, Ross, and Shanken (GRS) test for a factor model.�h]�h�OImplementation of the Gibbons, Ross, and Shanken (GRS) test for a factor model.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+h�dC:\Users\rpham\Desktop\FIN_Library\finance_byu\statistics.py:docstring of finance_byu.statistics.GRS�hKhj  hhubh �
field_list���)��}�(hhh]�(h �field���)��}�(hhh]�(h �
field_name���)��}�(h�
Parameters�h]�h�
Parameters�����}�(hhhj)  ubah}�(h]�h!]�h#]�h%]�h']�uh)j'  hj$  hj  hK ubh �
field_body���)��}�(hXW  **df: pandas.core.frame.DataFrame**
    DataFrame containing a columns with excess returns and columns with factor returns.

**exret: list**
    List of the variable / column names in :code:`df` which are excess portfolio returns to be tested.

**factors: list**
    List of the variable / column names in :code:`df` which are factor returns.
�h]�h �definition_list���)��}�(hhh]�(h �definition_list_item���)��}�(h�x**df: pandas.core.frame.DataFrame**
DataFrame containing a columns with excess returns and columns with factor returns.
�h]�(h �term���)��}�(h�#**df: pandas.core.frame.DataFrame**�h]�h �strong���)��}�(hjL  h]�h�df: pandas.core.frame.DataFrame�����}�(hhhjP  ubah}�(h]�h!]�h#]�h%]�h']�uh)jN  hjJ  ubah}�(h]�h!]�h#]�h%]�h']�uh)jH  hj  hKhjD  ubh �
definition���)��}�(hhh]�h,)��}�(h�SDataFrame containing a columns with excess returns and columns with factor returns.�h]�h�SDataFrame containing a columns with excess returns and columns with factor returns.�����}�(hjj  hjh  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj  hKhje  ubah}�(h]�h!]�h#]�h%]�h']�uh)jc  hjD  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jB  hj  hKhj?  ubjC  )��}�(h�s**exret: list**
List of the variable / column names in :code:`df` which are excess portfolio returns to be tested.
�h]�(jI  )��}�(h�**exret: list**�h]�jO  )��}�(hj�  h]�h�exret: list�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jN  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jH  hj  hKhj�  ubjd  )��}�(hhh]�h,)��}�(h�bList of the variable / column names in :code:`df` which are excess portfolio returns to be tested.�h]�(h�'List of the variable / column names in �����}�(h�'List of the variable / column names in �hj�  ubh7)��}�(h�
:code:`df`�h]�h�df�����}�(h�df�hj�  ubah}�(h]�h!]�hDah#]�h%]�h']�uh)h6hj�  ubh�1 which are excess portfolio returns to be tested.�����}�(h�1 which are excess portfolio returns to be tested.�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hj  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jc  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jB  hj  hKhj?  ubjC  )��}�(h�^**factors: list**
List of the variable / column names in :code:`df` which are factor returns.
�h]�(jI  )��}�(h�**factors: list**�h]�jO  )��}�(hj�  h]�h�factors: list�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jN  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jH  hj  hKhj�  ubjd  )��}�(hhh]�h,)��}�(h�KList of the variable / column names in :code:`df` which are factor returns.�h]�(h�'List of the variable / column names in �����}�(h�'List of the variable / column names in �hj�  ubh7)��}�(h�
:code:`df`�h]�h�df�����}�(h�df�hj�  ubah}�(h]�h!]�hDah#]�h%]�h']�uh)h6hj�  ubh� which are factor returns.�����}�(h� which are factor returns.�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hj  hKhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jc  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jB  hj  hKhj?  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j=  hj9  ubah}�(h]�h!]�h#]�h%]�h']�uh)j7  hj$  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j"  hj  hKhj  hhubj#  )��}�(hhh]�(j(  )��}�(h�Returns�h]�h�Returns�����}�(hhhj1  ubah}�(h]�h!]�h#]�h%]�h']�uh)j'  hj.  hj  hK ubj8  )��}�(h��output: tuple(float,float,Regtable)
    Tuple containing the GRS statistic, associated p-value, and Regtable with regression results for 
    regressions of the form exret = alpha + summation(beta*factor) + e.   












�h]�j>  )��}�(hhh]�jC  )��}�(h��output: tuple(float,float,Regtable)
Tuple containing the GRS statistic, associated p-value, and Regtable with regression results for 
regressions of the form exret = alpha + summation(beta*factor) + e.   












�h]�(jI  )��}�(h�#output: tuple(float,float,Regtable)�h]�h�#output: tuple(float,float,Regtable)�����}�(hjL  hjJ  ubah}�(h]�h!]�h#]�h%]�h']�uh)jH  hj  hK hjF  ubjd  )��}�(hhh]�h,)��}�(h��Tuple containing the GRS statistic, associated p-value, and Regtable with regression results for 
regressions of the form exret = alpha + summation(beta*factor) + e.�h]�h��Tuple containing the GRS statistic, associated p-value, and Regtable with regression results for 
regressions of the form exret = alpha + summation(beta*factor) + e.�����}�(hj]  hj[  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj  hKhjX  ubah}�(h]�h!]�h#]�h%]�h']�uh)jc  hjF  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jB  hj  hK hjC  ubah}�(h]�h!]�h#]�h%]�h']�uh)j=  hj?  ubah}�(h]�h!]�h#]�h%]�h']�uh)j7  hj.  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j"  hj  hKhj  hhubeh}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhNhNubh �comment���)��}�(h�!! processed by numpydoc !!�h]�h�!! processed by numpydoc !!�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)j�  hj  hhhj  hK#ubeh}�(h]�h!]�h#]�h%]�h']�uh)j	  hh�hhhh�hNubeh}�(h]�h!]�h#]�h%]�h']��domain��py��objtype��function��desctype�j�  �noindex��uh)h�hhhhShNhNubeh}�(h]��
statistics�ah!]�h#]��
statistics�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Example�h]�h�Example�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKubh �literal_block���)��}�(hX  >>> from finance_byu.statistics import GRS
>>> import numpy as np
>>> import pandas as pd
>>>
>>> n_periods = 1.0e2
>>>
>>> df = pd.DataFrame(np.random.random((int(n_periods),6)))
>>> df = df.rename(columns={0:'port1',1:'port2',2:'port3',3:'exmkt',4:'smb',5:'hml'})
>>> grsstat,pval,tbl = GRS(df,['port1','port2','port3'],['exmkt','smb','hml'])
>>> grsstat

29.257621230624505

>>> pval

1.8957078244964134e-13

>>> from tabulate import tabulate
>>> print(tabulate(tbl.render(),tablefmt='github',headers=tbl.render().columns))

|           | port1   | port2   | port3   |
|-----------|---------|---------|---------|
| Intercept | 0.431   | 0.574   | 0.452   |
|           | (4.78)  | (6.82)  | (5.14)  |
| exmkt     | -0.020  | 0.075   | 0.146   |
|           | (-0.19) | (0.79)  | (1.48)  |
| smb       | 0.094   | 0.051   | -0.135  |
|           | (0.91)  | (0.53)  | (-1.32) |
| hml       | 0.064   | -0.258  | 0.009   |
|           | (0.62)  | (-2.69) | (0.09)  |
| Obs       | 100     | 100     | 100     |
| Rsq       | 0.01    | 0.08    | 0.04    |�h]�hX  >>> from finance_byu.statistics import GRS
>>> import numpy as np
>>> import pandas as pd
>>>
>>> n_periods = 1.0e2
>>>
>>> df = pd.DataFrame(np.random.random((int(n_periods),6)))
>>> df = df.rename(columns={0:'port1',1:'port2',2:'port3',3:'exmkt',4:'smb',5:'hml'})
>>> grsstat,pval,tbl = GRS(df,['port1','port2','port3'],['exmkt','smb','hml'])
>>> grsstat

29.257621230624505

>>> pval

1.8957078244964134e-13

>>> from tabulate import tabulate
>>> print(tabulate(tbl.render(),tablefmt='github',headers=tbl.render().columns))

|           | port1   | port2   | port3   |
|-----------|---------|---------|---------|
| Intercept | 0.431   | 0.574   | 0.452   |
|           | (4.78)  | (6.82)  | (5.14)  |
| exmkt     | -0.020  | 0.075   | 0.146   |
|           | (-0.19) | (0.79)  | (1.48)  |
| smb       | 0.094   | 0.051   | -0.135  |
|           | (0.91)  | (0.53)  | (-1.32) |
| hml       | 0.064   | -0.258  | 0.009   |
|           | (0.62)  | (-2.69) | (0.09)  |
| Obs       | 100     | 100     | 100     |
| Rsq       | 0.01    | 0.08    | 0.04    |�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)j�  hKhj�  hhhh*ubeh}�(h]��example�ah!]�h#]��example�ah%]�h']�uh)h	hhhhhh*hKubeh}�(h]��statistics-module�ah!]�h#]��statistics module�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  h�h�j�  j�  u�	nametypes�}�(j�  Nj�  Nh��j�  Nuh}�(j�  hj�  hSh�h�j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.