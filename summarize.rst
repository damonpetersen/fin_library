Summarize Module
=================

The summarize module contains the :code:`summary` function which is useful for 
creating summary tables for DataFrames which contain portfolio returns.

:mod:`summarize`
-----------------------

.. autofunction:: finance_byu.summarize.summary


Examples
---------

::

    >>> import pandas as pd
    >>> import numpy as np
    >>> from finance_byu.summarize import summary
    >>> 
    >>> n_periods = 1.0e2
    >>> 
    >>> df = pd.DataFrame(np.random.random((100,3))/10.,columns=['Port 1','Port 2','Port 3'])
    >>> summary(df)

                 Port 1        Port 2        Port 3
    count  1.000000e+02  1.000000e+02  1.000000e+02
    mean   4.675320e-02  5.111644e-02  4.876299e-02
    std    2.888945e-02  2.707162e-02  2.930755e-02
    tstat  1.618348e+01  1.888193e+01  1.663837e+01
    pval   1.458893e-29  1.388467e-34  1.947348e-30
    min    1.270510e-03  2.812253e-05  1.322883e-03
    25%    1.849776e-02  2.927948e-02  2.236390e-02
    50%    4.430358e-02  5.130892e-02  4.726655e-02
    75%    7.027532e-02  7.172167e-02  7.522687e-02
    max    9.802267e-02  9.977429e-02  9.976381e-02    
