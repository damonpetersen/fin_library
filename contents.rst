.. Finance Library documentation master file, created by
   sphinx-quickstart on Sun Sep 29 13:40:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BYU FIN 585 Library Documentation
===========================================

This library contains functions and classes for FIN 585 at Brigham 
Young University which are generally useful for academic finance research. 

The library can currently be installed from PyPi:: 
    
    pip install finance-byu

To use this library, use the following code at the top of your script: :code:`import 
finance_byu as fin`. Or, to only import particular functions or classes use :code:`from 
finance_byu.<module> import <class or function>`. 

The Fama Macbeth module contains parallel implementations of Fama Macbeth regression functions. 
The rolling module contains rolling functions which make up for some speed and functionality 
deficits in :code:`pandas`. The Regtables module contains a class which facilitates access and presentation of grouped regression results. The summarize module contains summary functions useful for summarizing results of grouped regressions. The statistics module contains a function for computing the Gibbons, Ross, and Shanken (GRS) test for a factor model. The regression module contains a function for fixed effects regression similar to Stata's :code:`areg` function. 

.. toctree::
   :maxdepth: 2
   :caption: Modules:
   
   fama_macbeth
   rolling
   regtables
   summarize
   statistics
   regression
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
